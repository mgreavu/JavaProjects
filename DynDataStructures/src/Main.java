import dynamicDataStructures.SinglyLinkedList;

public class Main {

    public static void main(String[] args)
    {
        SinglyLinkedList list = new SinglyLinkedList();

        for ( int i=0; i<100; i++ )
        {
            if ( i %7 == 0)
            {
                list.Insert( i );
            }
        }

        System.out.println( "list size = " + list.Size() );
        list.Print();

        list.Find( 83 );
        list.Find( 21 );
    }
}
