package dynamicDataStructures;

/**
 * Created by mig on 5/22/2016.
 */
public class SinglyLinkedList
{
    class Node
    {
        public Node( int val )
        {
            m_val = val;
            m_next = null;
        }

        public Node getNext()
        {
            return m_next;
        }

        public int getVal()
        {
            return m_val;
        }

        public void setNext( Node node )
        {
            m_next = node;
        }

        private int m_val;
        private Node m_next;
    }

    Node m_head;
    int m_size;

    public SinglyLinkedList()
    {
        m_size = 0;
    }

    public int Size()
    {
        return m_size;
    }

    public void Print()
    {
        Node p = m_head;

        while( p != null )
        {
            System.out.print( p.getVal() + " " );
            p = p.getNext();
        }
        System.out.println();
    }

    public void Insert( int val )
    {
        if ( m_head == null )
        {
            m_head = new Node( val );
            m_size++;
            return;
        }

        Node p = m_head;
        while( p != null )
        {
            if ( p.getNext() == null )
            {
                p.setNext( new Node( val ) );
                m_size++;
                return;
            }
            p = p.getNext();
        }
    }

    public boolean Find( int searchFor )
    {
        Node p = m_head;

        while( p != null )
        {
            if ( p.getVal() == searchFor )
            {
                System.out.println( "Found value " + searchFor );
                return true;
            }
            p = p.getNext();
        }
        System.out.println( "Not found value " + searchFor );
        return false;
    }
}
